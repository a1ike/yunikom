"use strict";

jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });
  $('.open-drop').on('click', function (e) {
    e.preventDefault();
    $('.y-mob-drop').slideToggle('fast');
  });
  $('.y-mob-drop__close').on('click', function (e) {
    e.preventDefault();
    $('.y-mob-drop').slideToggle('fast');
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.y-modal').toggle();
  });
  $('.y-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'y-modal__centered') {
      $('.y-modal').hide();
    }
  });
  $('.y-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.y-modal').hide();
  });
  $('.open-search').on('click', function (e) {
    e.preventDefault();
    $('.y-search').toggle();
  });
  $('.y-search__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'y-search__centered') {
      $('.y-search').hide();
    }
  });
  $('.y-search__close').on('click', function (e) {
    e.preventDefault();
    $('.y-search').hide();
  });
  $('.y-modal__tab_1').on('click', function (e) {
    e.preventDefault();
    $('.y-modal__2').hide();
    $('.y-modal__1').show();
  });
  $('.y-modal__tab_2').on('click', function (e) {
    e.preventDefault();
    $('.y-modal__1').hide();
    $('.y-modal__2').show();
  });
  $('.y-header-basket-card__minus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });
  $('.y-header-basket-card__plus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });
  $('.y-basket-card__minus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });
  $('.y-basket-card__plus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });
  $('.y-good__minus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });
  $('.y-good__plus').on('click', function (e) {
    e.preventDefault();
    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });
  $('.y-header__link_basket').on('click', function (e) {
    e.preventDefault();
    $('.y-header-basket').toggle();
  });
  $('.y-header-basket__close').on('click', function (e) {
    e.preventDefault();
    $('.y-header-basket').hide();
  });
  $('.y-aside__dropper > a').on('click', function (e) {
    e.preventDefault();
    $(this).next().slideToggle('fast');
  });
  $(window).on('scroll', function () {
    var top = $(this).scrollTop();

    if (top > 50) {
      $('.y-mob-header__logo').addClass('y-mob-header__logo_scrolled');
    } else {
      $('.y-mob-header__logo').removeClass('y-mob-header__logo_scrolled');
    }
  });
  $(window).on('load', function () {
    if ($(window).width() < 1199) {
      $('.y-footer__link').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('y-footer__link_active');
        $(this).next().slideToggle('fast');
      });
      $('.y-aside__title').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('y-aside__title_active');
        $(this).next().slideToggle('fast');
      });
      $('.y-list__title').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('y-list__title_active');
        $('.y-list__wrapper').slideToggle('fast');
        $(this).next().slideToggle('fast');
      });
      new Swiper('.y-home-customers__cards', {
        centeredSlides: true,
        loop: true,
        slidesPerView: 1,
        allowTouchMove: false,
        autoplay: {
          delay: 0
        },
        loopedSlides: 12,
        freeMode: true,
        speed: 2000,
        spaceBetween: 30
      });
    }
  });
  $(window).on('load resize scroll', function (e) {
    if ($(window).width() < 1199) {
      $('.y-page-header__select').detach().prependTo('.y-aside_select');
      $('.y-footer__mob').detach().appendTo('.y-footer__help');
    }
  });
  new Swiper('.y-more .swiper-container', {
    spaceBetween: 10,
    slidesPerView: 'auto',
    centeredSlides: true,
    loop: true,
    // init: false,
    navigation: {
      nextEl: '.y-more .swiper-button-next',
      prevEl: '.y-more .swiper-button-prev'
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 40,
        centeredSlides: false
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 40,
        centeredSlides: false
      }
    }
  });
  $('.y-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');
    $('.y-tabs__header li').removeClass('current');
    $('.y-tabs__content').removeClass('current');
    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });
  new SimpleBar($('.y-header-basket__cards')[0]);
  var progressBar = new ProgressBar.Circle('#bar', {
    strokeWidth: 3,
    color: '#89ab31',
    trailColor: '#000000',
    trailWidth: 3,
    svgStyle: {
      display: 'block',
      // Important: make sure that your container has same
      // aspect ratio as the SVG canvas. See SVG canvas sizes above.
      width: '45px'
    }
  });
  new Swiper('.y-home-top', {
    pagination: {
      el: '.y-home-top .swiper-pagination',
      type: 'fraction'
    },
    navigation: {
      nextEl: '.y-home-top .swiper-button-next',
      prevEl: '.y-home-top .swiper-button-prev'
    },
    watchSlidesProgress: true,
    renderFraction: function renderFraction(currentClass, totalClass) {
      return '<span class="' + currentClass + '"></span>' + '/' + '<span class="' + totalClass + '"></span>';
    },
    on: {
      init: function init() {
        progressBar.animate(1 / this.slides.length);
      },
      slideChange: function slideChange() {
        progressBar.animate((this.slides.length - (this.slides.length - ++this.realIndex)) * (1 / this.slides.length));
      }
    }
  });
});
//# sourceMappingURL=main.js.map